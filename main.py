from typing import Optional
from fastapi import FastAPI
import pydantic
import uvicorn
import requests
from datetime import datetime as dt
import json

app = FastAPI()

EMOJI = {
    "finished": "🏁",
    "success": "✅",
    # "failed": "❌",
    # "canceled": "🚫",
    "started": "🔄",
    # "running": "🔄",
}


class NotificationMessage(pydantic.BaseModel):
    chat_id: int
    id: int


class NotificationManager:
    TOKEN = "5274656806:AAHTKLQOxb7z4_ctrdAfbqu-SZmZSZtGdLw"
    URL = f"https://api.telegram.org/bot{TOKEN}"
    CHAT_ID = "-1001657074877"

    @classmethod
    def get_new_message(cls) -> NotificationMessage:
        headers = {"content-type": "application/json;charset=utf-8"}
        body = {"chat_id": cls.CHAT_ID, "text": "Собираем информацию о новом пайплайне..."}
        json_body = json.dumps(body)
        response = requests.post(cls.URL + "/sendmessage", headers=headers, data=json_body).json()
        print(response)
        result = response["result"]
        chat_id = result["chat"]["id"]
        message_id = result["message_id"]
        return NotificationMessage(chat_id=chat_id, id=message_id)

    @classmethod
    def update_pipeline_message(cls, pipeline: "Pipeline"):
        print(
            f"Update message {pipeline.message.id} in chat {pipeline.message.chat_id}",
        )
        headers = {"content-type": "application/json;charset=utf-8"}
        body = {
            "chat_id": pipeline.message.chat_id,
            "message_id": pipeline.message.id,
            # "disable_web_page_preview": True,
            "text": str(pipeline),
        }
        json_body = json.dumps(body)
        response = requests.post(cls.URL + "/editmessagetext", headers=headers, data=json_body)
        print(response.json())


class JobMessage(pydantic.BaseModel):
    commit_message: str
    ref: str
    pipeline_link: str
    project_title: str
    job_name: str
    job_stage: str
    job_started_at: str
    username: str
    project_id: str
    pipeline_id: str


class Job(pydantic.BaseModel):
    name: str
    status: str
    last_updated: str
    # status: Union[Literal["started"], Literal["finished"]]

    def __str__(self):
        emoji_status = EMOJI[self.status]
        return f"{self.name} {self.last_updated} {emoji_status}"

    def __eq__(self, other):
        if isinstance(other, str):
            return self.name == other
        return self.name == other.name

    def update(self):
        self.last_updated = str(dt.now().time())


class Stage(pydantic.BaseModel):
    name: str
    jobs: list[Job]

    @property
    def status(self):
        finished = {"finished"} == set(map(lambda x: x.status, self.jobs))
        if finished:
            return "finished"
        return "started"

    def __str__(self):
        jobs_repr = []
        for job in self.jobs:
            jobs_repr.append(str(job))
        return f"[{self.name}] {EMOJI[self.status]}\n" + "\n".join(jobs_repr)

    def __contains__(self, other):
        return other in self.jobs

    def __eq__(self, other):
        if isinstance(other, str):
            return self.name == other
        return self.name == other.name

    def get_job(self, job_name: str) -> Optional[Job]:
        for job in self.jobs:
            if job_name == job:
                return job

    def new_job(self, job_name: str):
        job = Job(name=job_name, status="started", last_updated=str(dt.now().time()))
        self.jobs.append(job)
        return job

    def add_job(self, job: Job):
        self.jobs.append(job)


class Pipeline(pydantic.BaseModel):
    id: str
    message: NotificationMessage
    username: str
    project_title: str
    link: str
    ref: str
    commit_message: str
    stages: list[Stage]

    @property
    def status(self):
        finished = {"finished"} == set(map(lambda x: x.status, self.stages))
        if finished:
            return "finished"
        return "started"

    def __contains__(self, other):
        return other in self.stages

    def get_stage(self, stage_name: str) -> Optional[Stage]:
        for stage in self.stages:
            if stage == stage_name:
                return stage

    def new_stage(self, stage_name: str) -> Stage:
        stage = Stage(name=stage_name, jobs=[])
        self.stages.append(stage)
        return stage

    def add_stage(self, stage: Stage):
        self.stages.append(stage)

    def __str__(self):
        title = [self.project_title + f" {EMOJI[self.status]}", self.ref, self.link, "by " + self.username]
        title_repr = "\n".join(title)
        stages_repr = "\n\n".join([str(stage) for stage in self.stages])
        return title_repr + "\n\n" + stages_repr


class PipelineStorage:
    _storage: dict[str, Pipeline] = {}

    @classmethod
    def register_pipeline(cls, job_message: JobMessage):
        if job_message.pipeline_id in cls._storage:
            return cls._storage[job_message.pipeline_id]
        pipeline = Pipeline(
            id=job_message.pipeline_id,
            message=NotificationManager.get_new_message(),
            username=job_message.username,
            project_title=job_message.project_title,
            link=job_message.pipeline_link,
            ref=job_message.ref,
            commit_message=job_message.commit_message,
            stages=[],
        )
        cls._storage[pipeline.id] = pipeline
        PipelineStorage.update_pipeline(job_message)
        return pipeline

    @classmethod
    def update_pipeline(cls, job_message: JobMessage):
        pipeline = cls.get_pipeline(job_message.pipeline_id)

        if job_message.job_stage not in pipeline:
            pipeline.new_stage(job_message.job_stage)

        stage = pipeline.get_stage(job_message.job_stage)

        finished = True
        if job_message.job_name not in stage:
            finished = False
            stage.new_job(job_message.job_name)
        job = stage.get_job(job_message.job_name)
        # assume the first time job appeared - started
        # next times - finished
        if finished:
            job.status = "finished"
            job.update()
        return pipeline

    def __contains__(self, id: str):
        return id in PipelineStorage._storage

    @classmethod
    def get_pipeline(cls, id: str):
        return cls._storage[id]

    @classmethod
    def unregister_pipeline(cls, pipeline: Pipeline):
        cls._storage.pop(pipeline.id)


@app.post("/notify")
async def notify(message: JobMessage) -> str:
    if message.pipeline_id in PipelineStorage():
        pipeline = PipelineStorage.update_pipeline(message)
    else:
        pipeline = PipelineStorage.register_pipeline(message)
    NotificationManager.update_pipeline_message(pipeline)
    return "ok"


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
